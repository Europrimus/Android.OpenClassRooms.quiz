package fr.courty.didier.openclassroom.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import fr.courty.didier.openclassroom.model.user;
import fr.courty.didier.openclassroom.R;

public class MainActivity extends AppCompatActivity {

    private EditText mNameInput;
    private Button mPlayButton;
    private TextView mScoreString;
    private user mUser;
    // unidentifiant d'activitée
    private static final int GAME_ACTIVITY_REQUEST_CODE = 42;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // on initialise un utilisateur vide
        mUser = new user();

        // on lie les variable à la vue
        mNameInput = (EditText) findViewById(R.id.activity_main_name_input);
        mPlayButton = (Button) findViewById(R.id.activity_main_play_btn);
        mScoreString = (TextView) findViewById(R.id.activity_main_score_string);

        // on désactive le bouton "jouer"
        mPlayButton.setEnabled(false);

        // on surveille les mofication du champ de text
        mNameInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mPlayButton.setEnabled(s.toString().length() > 3);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        // on surveille le clic sur le bouton
        mPlayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // enregistrement du nom de l'utilisateur
                mUser.setName( mNameInput.getText().toString() );

                // changement d'affichage
                Intent gameActivity = new Intent(MainActivity.this, GameActivity.class);

                //startActivity(gameActivity);
                startActivityForResult(gameActivity, GAME_ACTIVITY_REQUEST_CODE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (GAME_ACTIVITY_REQUEST_CODE == requestCode && RESULT_OK == resultCode) {
            // Fetch the score from the Intent
            int score = data.getIntExtra(GameActivity.BUNDLE_EXTRA_SCORE, 0);

            mScoreString.setText( mUser.getName() + " : " + score);
        }
    }
}
