package fr.courty.didier.openclassroom.model;

import java.util.List;

public class ask {
    private String mQuestion;
    private List<String> mChoiceList;
    private int mAnswerIndex;

    public ask(String question, List<String> choiceList, int answerIndex) {
        setQuestion( question );
        setChoiceList( choiceList );
        setAnswerIndex( answerIndex );
    }

    public String getQuestion() {
        return mQuestion;
    }

    public void setQuestion(String question) {
        mQuestion = question;
    }

    public List<String> getChoiceList() {
        return mChoiceList;
    }

    public void setChoiceList(List<String> choiceList) {
        mChoiceList = choiceList;
    }

    public int getAnswerIndex() {
        return mAnswerIndex;
    }

    public void setAnswerIndex(int answerIndex) {
        if ( answerIndex >= 0 && answerIndex < mChoiceList.size())
            mAnswerIndex = answerIndex;
    }
}
