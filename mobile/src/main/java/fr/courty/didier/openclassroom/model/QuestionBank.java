package fr.courty.didier.openclassroom.model;

import java.util.Collections;
import java.util.List;

import fr.courty.didier.openclassroom.model.ask;

public class QuestionBank {
    private List<ask> mQuestionList;
    private int mNextQuestionIndex;

    public QuestionBank(List<ask> questionList) {
        // Shuffle the question list before storing it
        // on mélange les questions
        Collections.shuffle(questionList);
        mQuestionList = questionList;
        // on met l'index à 0
        mNextQuestionIndex = 0;
    }


    public ask getQuestion() {
        // Loop over the questions and return a new one at each call
        // si on est trop loin dans la liste de question
        if ( mNextQuestionIndex >= mQuestionList.size() ) {
            mNextQuestionIndex = 0;
        }
        // on revoi la question et on incremente le compteur
        return mQuestionList.get(mNextQuestionIndex++);
    }
}
