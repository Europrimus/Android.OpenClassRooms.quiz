package fr.courty.didier.openclassroom.controller;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;

import fr.courty.didier.openclassroom.R;
import fr.courty.didier.openclassroom.model.QuestionBank;
import fr.courty.didier.openclassroom.model.ask;

public class GameActivity extends AppCompatActivity implements View.OnClickListener{

    // les variables
    private Button mAnswer1;
    private Button mAnswer2;
    private Button mAnswer3;
    private Button mAnswer4;
    private TextView mQuestion;
    private QuestionBank mQuestionBank;
    private ask mAsk;
    private int mNumberOfQuestions;
    private int mScore;

    public static final String BUNDLE_EXTRA_SCORE = "BUNDLE_EXTRA_SCORE";


    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        // nombre de questions
        mNumberOfQuestions = 4;
        mScore = 0;

        // on lie les variable à la vue
        mAnswer1 = (Button) findViewById(R.id.activity_game_answer1_btn);
        mAnswer2 = (Button) findViewById(R.id.activity_game_answer2_btn);
        mAnswer3 = (Button) findViewById(R.id.activity_game_answer3_btn);
        mAnswer4 = (Button) findViewById(R.id.activity_game_answer4_btn);
        mQuestion = (TextView) findViewById(R.id.activity_game_ask_text);

        // Use the same listener for the four buttons.
        // The tag value will be used to distinguish the button triggered
        mAnswer1.setOnClickListener(this);
        mAnswer2.setOnClickListener(this);
        mAnswer3.setOnClickListener(this);
        mAnswer4.setOnClickListener(this);

        // Use the tag property to 'name' the buttons
        mAnswer1.setTag(0);
        mAnswer2.setTag(1);
        mAnswer3.setTag(2);
        mAnswer4.setTag(3);

        mQuestionBank = this.generateQuestions();

        mAsk = mQuestionBank.getQuestion();
        displayQuestion(mAsk);
    }

    private void displayQuestion(final ask question) {
        // Set the text for the question text view and the four buttons
        mQuestion.setText( question.getQuestion() );
        mAnswer1.setText( question.getChoiceList().get(0) );
        mAnswer2.setText( question.getChoiceList().get(1) );
        mAnswer3.setText( question.getChoiceList().get(2) );
        mAnswer4.setText( question.getChoiceList().get(3) );

    }

    private QuestionBank generateQuestions() {
        ask question1 = new ask("Qui est le créateur d'Android?",
                Arrays.asList("Andy Rubin",
                        "Steve Wozniak",
                        "Jake Wharton",
                        "Paul Smith"),
                0);

        ask question2 = new ask("En quelle année le première homme a-t-il marché sur la lune?",
                Arrays.asList("1958",
                        "1962",
                        "1967",
                        "1969"),

                3);

        ask question3 = new ask("Quelle est le numéro de la maison des Simpsons?",
                Arrays.asList("42",
                        "101",
                        "666",
                        "742"),
                3);


        return new QuestionBank(Arrays.asList(question1,
                question2,
                question3));
    }

    @Override
    public void onClick(View v) {
        // on vérifie la réponse
        int responseIndex = (int) v.getTag();
        if ( responseIndex == mAsk.getAnswerIndex() ) {
            Toast.makeText(this, getResources().getString(R.string.scoreTrue), Toast.LENGTH_SHORT).show();
            mScore++;
        } else {
            Toast.makeText(this, getResources().getString(R.string.scoreFalse), Toast.LENGTH_SHORT).show();
        };

        // on attend 2 seconde
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // on passe à la question suivante
                // on décrémente le numbre de questions restante
                if (--mNumberOfQuestions == 0) {
                    // No question left, end the game
                    afficheScrore();
                } else {
                    mAsk = mQuestionBank.getQuestion();
                    displayQuestion(mAsk);
                }
            }
        }, 2000);


    }

    // affichage d'une boite de dialogue
    private void afficheScrore() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle( getResources().getString(R.string.scoreFini) )
                .setMessage( getResources().getString(R.string.scoreShow) +" : " + mScore)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // créationd'un intent
                        Intent intent = new Intent();
                        // ajout du score
                        intent.putExtra(BUNDLE_EXTRA_SCORE, mScore);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                })
                .create()
                .show();
    }
}
